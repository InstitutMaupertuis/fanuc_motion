#include <ros/ros.h>
#include <eigen_conversions/eigen_msg.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit_msgs/ExecuteKnownTrajectory.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <visualization_msgs/Marker.h>

#include <Eigen/StdVector>

/** @brief Transforms degrees into radians */
#define DEG2RAD(x) ((x)*0.017453293)
/** @brief Transforms radians into degrees */
#define RAD2DEG(x) ((x)*57.29578)

/** @brief Generate a user defined number of poses on a grid
 * @param[in] start_pose The pose at the bottom right corner (top view of the robot, facing towards)
 * @param[out] robot_trajectory Vector of robot poses to be filled
 * @param[in] x_gap Size of each step along X axis
 * @param[in] x_steps Number of steps along X axis
 * @param[in] y_gap Size of each step along Y axis
 * @param[in] y_steps Number of steps along Y axis
 * @note Assumes the cube face is horizontal, orientation = identity matrix
 * The points are generated on Y first.
 */
void generateGridPoses(const Eigen::Isometry3d &start_pose,
                       std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d> > &robot_poses,
                       const double x_gap,
                       const unsigned int x_steps,
                       const double y_gap,
                       const unsigned int y_steps)
{
  robot_poses.resize(x_steps * y_steps);
  robot_poses[0] = start_pose;

  unsigned int x_count = 1;
  unsigned int y_count = 1;

  for (size_t i = 1; i < robot_poses.size(); i++) // Start at 1, pose 0 is already OK
  {
    if (y_count < y_steps)
    {
      if (x_count % 2 == 1)
        robot_poses[i].translation() = robot_poses[i - 1].translation() + Eigen::Vector3d(0, y_gap, 0);
      else
        robot_poses[i].translation() = robot_poses[i - 1].translation() + Eigen::Vector3d(0, -y_gap, 0);
      y_count++;
    }
    else
    {
      y_count = 1;
      if (x_count % 2 == 1)
        robot_poses[i].translation() = robot_poses[0].translation()
            + Eigen::Vector3d(x_gap * x_count, y_gap * (y_steps - 1), 0);
      else
        robot_poses[i].translation() = robot_poses[0].translation() + Eigen::Vector3d(x_gap * x_count, 0, 0);
      x_count++;
    }

    robot_poses[i].linear() = start_pose.linear();
  }
}

/** @brief The main function
 * @param[in] argc
 * @param[in] argv
 * @return Exit status */
int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "planar_swipe");
  ros::NodeHandle node;
  tf::TransformListener listener(node);

  // Initialize move group
  moveit::planning_interface::MoveGroupInterface group("manipulator");
  group.setPoseReferenceFrame("base_link");
  ros::AsyncSpinner spinner(1);
  spinner.start();

  // Create a topic with a marker describing the sphere of calibration
  ros::Publisher marker_pub = node.advertise<visualization_msgs::Marker>("cube", 1, true);
  visualization_msgs::Marker marker;
  marker.header.frame_id = "/base_link";
  marker.header.stamp = ros::Time::now();
  marker.ns = "basic_shapes";
  marker.id = 0;

  marker.type = visualization_msgs::Marker::CUBE;
  marker.action = visualization_msgs::Marker::ADD;
  // Marker scale
  marker.scale.x = 0.5;
  marker.scale.y = 0.5;
  marker.scale.z = 0.5;
  // Marker pose
  marker.pose.position.x = 1.0;
  marker.pose.position.y = 0.0;
  marker.pose.position.z = marker.scale.z / 2.; // Half of cube size
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  // Marker color
  marker.color.r = 0.6f;
  marker.color.g = 0.6f;
  marker.color.b = 0.6f;
  marker.color.a = 1.0;
  marker.lifetime = ros::Duration();
  while (marker_pub.getNumSubscribers() < 1)
  {
    ROS_WARN_STREAM_ONCE("No subscriber to the marker: Please create a subscriber in rviz" << std::endl);
    ros::Duration(1).sleep();
  }
  marker_pub.publish(marker);

  std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d> > way_points_vector;
  Eigen::Isometry3d start_pose;
  start_pose.matrix() <<
      1, 0, 0, marker.pose.position.x - marker.scale.x / 2.,
      0, -1, 0, marker.pose.position.y - marker.scale.y / 2.,
      0, 0, -1, marker.pose.position.z + marker.scale.z / 2. + 0.1,
      0, 0, 0, 1;

  generateGridPoses(start_pose, way_points_vector, 0.1, 6, 0.5, 2);

  // Copy the vector of Eigen poses into a vector of ROS poses
  std::vector<geometry_msgs::Pose> way_points_msg;
  way_points_msg.resize(way_points_vector.size());
  for (size_t i = 0; i < way_points_msg.size(); i++)
    tf::poseEigenToMsg(way_points_vector[i], way_points_msg[i]);

  moveit::planning_interface::MoveGroupInterface::Plan plan;
  double result(group.computeCartesianPath(way_points_msg, 0.05, 15, plan.trajectory_));
  if (result == -1)
  {
    ROS_ERROR_STREAM("Path planning failed");
    return 1;
  }

  if (!group.execute(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS)
  {
    ROS_ERROR_STREAM("Could not execute planned trajectory");
    return 1;
  }

  ros::Duration(3).sleep();
  return 0;
}
