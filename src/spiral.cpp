#include <ros/ros.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf/transform_listener.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit_msgs/ExecuteKnownTrajectory.h>

typedef std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d> > RobotPoses;

int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "cube");
  ros::NodeHandle node;
  tf::TransformListener listener;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  moveit::planning_interface::MoveGroupInterface group("manipulator");
  group.setPoseReferenceFrame("base"); // Otherwise "base_link" is the reference!
  group.setPlannerId("RRTConnectkConfigDefault");
  group.setPlanningTime(1);

  // Create a trajectory (vector of Eigen poses)
  std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d> > way_points_vector;
  Eigen::Isometry3d pose(Eigen::Isometry3d::Identity());

  pose.linear() <<
      1, 0, 0,
      0, -1, 0,
      0, 0, -1;
  pose.translation() << 0.8, 0.0, 0.3;
  way_points_vector.push_back(pose);

  // Logarithmic spiral
  // Spiral parameters
  double a(0.015), b(0.04); // a = size

  // Ensure constant points sampling during time
  double beginning_sampling = 0.4;
  double end_sampling = 0.03;
  const unsigned int number_of_points(100);

  double time(0.0);
  for (size_t i = 0; i < number_of_points; ++i)
  {
    // Sampling time is proportional to i
    double time_increment = beginning_sampling + i * ((end_sampling - beginning_sampling) / number_of_points);
    time += time_increment;

    Eigen::Vector3d new_pose;
    new_pose[0] = a * exp(b * time) * cos(time);
    new_pose[1] = a * exp(b * time) * sin(time);
    new_pose[2] = 0.0;
    pose.translation() = new_pose + way_points_vector[0].translation();
    way_points_vector.push_back(pose);
  }
  way_points_vector.push_back(way_points_vector[0]);

  // Copy the vector of Eigen poses into a vector of ROS poses
  std::vector<geometry_msgs::Pose> way_points_msg(1);
  tf::poseEigenToMsg(way_points_vector[0], way_points_msg[0]);

  // Execute trajectory
  moveit::planning_interface::MoveGroupInterface::Plan plan;
  double result(group.computeCartesianPath(way_points_msg, 0.05, 15, plan.trajectory_));
  if (result == -1)
  {
    ROS_ERROR_STREAM("Path planning failed");
    return 1;
  }

  if (!group.execute(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS)
  {
    ROS_ERROR_STREAM("Could not execute planned trajectory");
    return 1;
  }

  // Copy the vector of Eigen poses into a vector of ROS poses
  way_points_msg.resize(way_points_vector.size());
  for (size_t i = 0; i < way_points_msg.size(); i++)
    tf::poseEigenToMsg(way_points_vector[i], way_points_msg[i]);

  result = group.computeCartesianPath(way_points_msg, 0.05, 15, plan.trajectory_);
  if (result == -1)
  {
    ROS_ERROR_STREAM("Path planning failed");
    return 1;
  }

  if (!group.execute(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS)
  {
    ROS_ERROR_STREAM("Could not execute planned trajectory");
    return 1;
  }

  ros::Duration(3).sleep();
  return 0;
}

