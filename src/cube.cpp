#include <vector>
#include <ros/ros.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf/transform_listener.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit_msgs/ExecuteKnownTrajectory.h>

typedef std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d> > RobotPoses;

int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "cube");
  ros::NodeHandle node;
  tf::TransformListener listener;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  moveit::planning_interface::MoveGroupInterface group("manipulator");
  group.setPoseReferenceFrame("base"); // Otherwise "base_link" is the reference!
  group.setPlannerId("RRTConnectkConfigDefault");
  group.setPlanningTime(1);

  // Create a trajectory (vector of Eigen poses)
  std::vector<Eigen::Isometry3d, Eigen::aligned_allocator<Eigen::Isometry3d> > way_points_vector;
  Eigen::Isometry3d pose(Eigen::Isometry3d::Identity());

  // Square belonging to XY plane
  pose.linear() <<
      1, 0, 0,
      0, -1, 0,
      0, 0, -1;
  pose.translation() << 1.1, -0.1, -0.1;
  way_points_vector.push_back(pose);
  pose.translation() << 1.3, -0.1, -0.1;
  way_points_vector.push_back(pose);
  pose.translation() << 1.3, 0.1, -0.1;
  way_points_vector.push_back(pose);
  pose.translation() << 1.1, 0.1, -0.1;
  way_points_vector.push_back(pose);
  way_points_vector.push_back(way_points_vector[0]);

  // Copy the vector of Eigen poses into a vector of ROS poses
  std::vector<geometry_msgs::Pose> way_points_msg;
  way_points_msg.resize(way_points_vector.size());
  for (size_t i = 0; i < way_points_msg.size(); i++)
    tf::poseEigenToMsg(way_points_vector[i], way_points_msg[i]);

  // Execute trajectory
  moveit::planning_interface::MoveGroupInterface::Plan plan;
  double result(group.computeCartesianPath(way_points_msg, 0.05, 15, plan.trajectory_));
  if (result == -1)
  {
    ROS_ERROR_STREAM("Path planning failed");
    return 1;
  }

  if (!group.execute(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS)
  {
    ROS_ERROR_STREAM("Could not execute planned trajectory");
    return 1;
  }

  ros::Duration(1).sleep();
  listener.waitForTransform("/base", "/tool0", ros::Time::now(), ros::Duration(1.5)); // Wait for the current robot pose to come
  for (size_t i = 0; i < way_points_msg.size(); i++)
    way_points_msg[i].position.z -= 0.1; // Z offset of 10 centimeters

  listener.waitForTransform("/base", "/tool0", ros::Time::now(), ros::Duration(1.5));

  result = group.computeCartesianPath(way_points_msg, 0.05, 15, plan.trajectory_);
  if (result == -1)
  {
    ROS_ERROR_STREAM("Path planning failed");
    return 1;
  }

  if (!group.execute(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS)
  {
    ROS_ERROR_STREAM("Could not execute planned trajectory");
    return 1;
  }

  ros::Duration(1).sleep();
  listener.waitForTransform("/base", "/tool0", ros::Time::now(), ros::Duration(1.5)); // Wait for the current robot pose to come
  std::vector<double> target;
  target.push_back(0.0);
  target.push_back(0.0);
  target.push_back(0.0);
  target.push_back(0.0);
  target.push_back(-1.5707);
  target.push_back(0.0);
  group.setJointValueTarget(target);
  group.move();

  ros::Duration(3).sleep();
  return 0;
}

