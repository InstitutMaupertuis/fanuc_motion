#include <vector>
#include <ros/ros.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf/transform_listener.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_interface/planning_interface.h>

/** @brief The main function
 * @param[in] argc
 * @param[in] argc
 * @return Exit status */
int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "initial_joint_state");
  ros::NodeHandle node;

  ros::AsyncSpinner spinner(1);
  spinner.start();

  tf::TransformListener listener;
  std::string frame = "base";

  moveit::planning_interface::MoveGroupInterface group("manipulator");
  Eigen::Isometry3d robot_pose;
  listener.waitForTransform(frame, "/tool0", ros::Time::now(), ros::Duration(1.0));
  tf::poseMsgToEigen(group.getCurrentPose().pose, robot_pose);
  std::cout << "Current robot pose:\n" << robot_pose.matrix() << std::endl;

  std::vector<double> joints(group.getCurrentJointValues());
  for (unsigned i = 0; i < joints.size(); ++i)
    std::cout << "J" << i << ": " << joints[i] << std::endl;

  ros::waitForShutdown();
  spinner.stop();
  return 0;
}
