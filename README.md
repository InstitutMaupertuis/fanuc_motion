[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

This ROS package is a simple package using Fanuc robot definitions, the programs loads a Fanuc robot, makes it move and terminate.

# How to compile
Create a catkin workspace and clone the project:

```bash
mkdir -p catkin_workspace
cd catkin_workspace
git clone https://gitlab.com/InstitutMaupertuis/fanuc_motion.git
wstool init src fanuc_motion/fanuc_motion.rosinstall
mv fanuc_motion src
```

Resolve ROS dependencies:
```bash
rosdep install --from-paths src --ignore-src --rosdistro melodic -y
```
Compile:
```bash
catkin_make
```

# Testing the robot movement
```bash
roslaunch fanuc_motion cube.launch
roslaunch fanuc_motion initial_joint_state.launch
roslaunch fanuc_motion planar_swipe.launch
roslaunch fanuc_motion spiral.launch
```
